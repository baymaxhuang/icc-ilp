/* This file shows how to model a virtual network embedding problem in GMPL. */
/* VNE considering flow table resource constraints intermediate nodes */

/* written by Huibai Huang */

# result file
param result_file, symbolic := "result.txt";
param result_rho, symbolic := ".\solution\rho.txt";
param result_delta, symbolic := ".\solution\delta.txt";
param result_omega, symbolic := ".\solution\omega.txt";
param result_flowtable, symbolic := ".\solution\flowtable.txt";

set V;
/* set of substrate vertices */


set VV;
/* virtual nodes */


param c{u in V};
/* cpu of node u */

param t{u in V};
/* flow table  of node u */

param b{u in V, v in V  };
/* bandwidth of edge (u,v) */

param linkcon1{u in V, v in V};
/* parameter of rho constraint */

param linkcon2{i in VV, j in VV};
/* parameter of rho constraint */

param cd{i in VV};
/* cpu demand of virtual node VV(i) */

param td{i in VV,j in VV};
/* flow table demand of virtual link VV(i,j) */

param tx{i in VV,j in VV};
/* extra flowtable demand of ingress node of substrate path representing the virtual link VV(i,j) */

param bd{i in VV,j in VV};
/* bandwidth demand of virtual link VE(i,j) */

param tB;
/* total bandwidth */
param tC;
/* total CPU*/
param tT;
/* total tables*/

var del{i in VV, u in V}, binary;
/* indicator variable */

var rho{i in VV,j in VV, u in V, v in V}, binary;
/* indicator variable */

var omega{i in VV,j in VV, u in V}, binary;

minimize cost: (sum{i in VV,j in VV, u in V, v in V} rho[i,j,u,v]*bd[i,j]) / tB + (sum{i in VV, u in V} del[i,u] * cd[i]) / tC
                    + ( sum{u in V, i in VV,j in VV} ( (omega[i,j,u] + del[i,u] + del[j,u])*td[i,j]  + (del[i,u] + del[j,u])*tx[i,j] ) ) / tT;

/*minimize cost: (sum{i in VV,j in VV, u in V, v in V} rho[i,j,u,v]*bd[i,j]) / tB + (sum{i in VV, u in V} del[i,u] * cd[i]) / tC;*/
/*minimum cost resource*/

s.t. bwcon1{u in V,v in V}: b[u,v] - sum{i in VV, j in VV} rho[i,j,u,v] * bd[i,j] >=0;
/* bandwidth constraint*/

s.t. cpucon2{u in V}: c[u] - sum{i in VV} del[i,u] * cd[i] >=0;
/* cpu constraint*/

s.t. tablecon3{u in V}: t[u] - sum{i in VV, j in VV} ( (omega[i,j,u] + del[i,u] + del[j,u])*td[i,j]  + (del[i,u] + del[j,u])*tx[i,j] ) >=0;
/*flow table constraint*/

s.t. node1{i in VV}: sum{u in V} del[i,u] = 1;
s.t. node2{u in V}: sum{i in VV} del[i,u] <=1;

s.t. flowcon1{u in V, i in VV, j in VV}: sum{v in V} rho[i,j,u,v] - sum{v in V} rho[i,j,v,u] = (del[i,u] - del[j,u])*linkcon2[i,j];
/*flow conservation constraints */

s.t. pathcon1 { u in V, v in V, i in VV, j in VV}: rho[i,j,u,v] = rho[j,i,v,u];

s.t. intercon1{u in V, i in VV, j in VV diff {i}}:sum{v in V diff {u} } rho[i,j,u,v] - omega[i,j,u] = del[i,u]*linkcon2[i,j];

s.t. rhocon1 { u in V, v in V, i in VV, j in VV}: rho[i,j,u,v] <= linkcon1[u,v];
s.t. rhocon2 { u in V, v in V, i in VV, j in VV}: rho[i,j,u,v] <= linkcon2[i,j];
s.t. omegacon1 { u in V, i in VV, j in VV}: omega[i,j,u] <=  linkcon2[i,j];

solve;

printf "variable list:\n" > result_file;

printf "V:\n" >> result_file;
printf  {u in V}: "%i ", u >> result_file;
printf "\n" >> result_file;

printf "VV:\n" >> result_file;
printf  {i in VV}: "%i ", i >> result_file;
printf "\n" >> result_file;

printf "c:\n" >> result_file;
printf  {u in V}: "%i ", c[u] >> result_file;
printf "\n" >> result_file;

printf "t:\n" >> result_file;
printf  {i in V}: "%i ", t[i] >> result_file;
printf "\n" >> result_file;

printf "b:\n" >> result_file;
printf  {u in V, v in V}: "%i ", b[u,v] >> result_file;
printf "\n" >> result_file;

printf "tT:\n" >> result_file;
printf  "%i ", tT >> result_file;
printf "\n" >> result_file;

printf "del:\n" >> result_file;
printf  {i in VV, u in V: del[i,u] > 0}: "%i %i %i\n", i, u, del[i,u] >> result_file;
printf  {i in VV, u in V: del[i,u] > 0}: "%i %i %i\n", i, u, del[i,u] > result_delta;

printf "rho:\n" >> result_file;
printf  {i in VV, j in VV, u in V, v in V: rho[i,j,u,v] > 0}: "%i %i %i %i %i\n", i, j, u, v, rho[i,j,u,v] >> result_file;
printf  {i in VV, j in VV, u in V, v in V: rho[i,j,u,v] > 0}: "%i %i %i %i %i\n", i, j, u, v, rho[i,j,u,v] > result_rho;

printf "omega:\n" >> result_file;
printf {i in VV, j in VV, u in V: omega[i,j,u] > 0}: "%i %i %i %i\n",i ,j, u, omega[i,j,u] >> result_file;
printf {i in VV, j in VV, u in V: omega[i,j,u] > 0}: "%i %i %i %i\n",i ,j, u, omega[i,j,u] > result_omega;

printf "usedbandwidth: %f \n", sum{i in VV,j in VV, u in V, v in V} rho[i,j,u,v]*bd[i,j] >> result_file;
printf "usedcpu: %f \n", sum{i in VV, u in V} del[i,u] * cd[i] >> result_file;
printf "usedflowtable1: %f \n",  sum{u in V, i in VV,j in VV} (omega[i,j,u] + del[i,u] + del[j,u])*td[i,j] >> result_file;
printf "usedflowtable2: %f \n",  sum{u in V, i in VV,j in VV} ((omega[i,j,u] + del[i,u] + del[j,u])*td[i,j]  + (del[i,u] + del[j,u])*tx[i,j] ) >> result_file;
printf "usedflowtableofnoede5: %f \n",  sum{ i in VV,j in VV} ((omega[i,j,5] + del[i,5] + del[j,5])*td[i,j]  + (del[i,5] + del[j,5])*tx[i,j] ) >> result_file;
printf "usedflowtableofnoede6: %f \n",  sum{ i in VV,j in VV} ((omega[i,j,6] + del[i,6] + del[j,6])*td[i,j]  + (del[i,6] + del[j,6])*tx[i,j] ) >> result_file;

printf {u in V: sum{ i in VV,j in VV} ((omega[i,j,u] + del[i,u] + del[j,u])*td[i,j]  + (del[i,u] + del[j,u])*tx[i,j] ) > 0 }: "%i %i\n", u, sum{ i in VV,j in VV} ((omega[i,j,u] + del[i,u] + del[j,u])*td[i,j]  + (del[i,u] + del[j,u])*tx[i,j] ) > result_flowtable;

end;
