clear
curFilename = mfilename('fullpath');
[pathstr, name, ext] = fileparts(curFilename);
reqPath = [pathstr,'\test_2_10'];
dirList = dir(reqPath);

% lambda = 5; % average arrival rate is 1 requests per 5 time units
mu = 1000; % average holding time
% erlangs list of the requests
erlangs_List = [2 5 8 10 12 15 18];
N=4;
% parameter settings
%virtual request_queue parameter
band_min = 5; % minimum bandwidth requirement
band_max = 20;% maximum bandwidth requirement

cpu_min = 0;% minimum cpu requirement
cpu_max = 20;% minimum maximum requirement

table_min = 5;
table_max = 15;
% generate the virtual network requests
req_set = 1;
fprintf('starting to generate requests ....\n\n');

req_set_num = min(length(erlangs_List) + 2,length(dirList));
for req_set_id = 1:req_set_num
    
    topo_filename = [reqPath,'\',dirList(req_set_id).name];
    [pathstr, name, ext] = fileparts(topo_filename);
    
    if strcmp(ext,'.txt') == 0
        continue;
    end
    
    %
    % mu = erlangs_List(req_set) * lambda; % fixed arrival rate, change
    % average holding time
    lambda = mu / erlangs_List(req_set); % fixed average holding time, change arrival rate
    
    % generate the arrival times
    arrival_times = zeros(N,1);
    arrival_times(1) = 1;
    for index = 2:N
        arrival_times(index) = arrival_times(index - 1) + random('exponential',lambda);
    end
    arrival_times = round(arrival_times);
    
    % generate the holding times
    duration_times = random('exponential',mu,N,1);
    duration_times = round(duration_times);
    
    %
    fprintf('open txt file: %s\n',topo_filename);
    fp_topo = fopen(topo_filename,'r');
    tline = fgetl(fp_topo);
    
    req_id = 1;
    partten1 = 'SIZES (number-of-nodes number-of-edges)';
    partten3 = 'EDGES (from-node to-node)';
    partten2 = 'LOCATIONS (x,y)';
    
    %selected_times = zeros(1,sub_num_nodes); % the parameter to record the selected times of each substrate nodes
    while (~feof(fp_topo) && req_id <= N)
        while (strncmp(tline,partten1,2) == 0 && ~feof(fp_topo))
            tline = fgetl(fp_topo);
        end
        tline = fgetl(fp_topo);
        str = deblank(tline);
        A = regexp(str, '\s+', 'split');
        
        num_nodes = str2num(A{1});
        num_links = str2num(A{2});
        
        while(strncmp(tline,partten3,2) == 0 && ~feof(fp_topo))
            tline = fgetl(fp_topo);
        end
        from = zeros(num_links,1);
        to = zeros(num_links,1);
        bandwidth = zeros(num_links,1);
        table = zeros(num_links,1);
        
        for i = 1:num_links
            tline = fgetl(fp_topo);
            
            str = deblank(tline);
            
            B = regexp(str, '\s+', 'split');
            from(i) = str2num(B{1}) + 1;
            to(i) = str2num(B{2})  + 1;
            bandwidth(i) = round(rand() * (band_max - band_min) + band_min);%bandwidth demand of every virtual link
            table(i) = round(rand() * (table_max - table_min) + table_min);
            
        end
        band_demand = full(sparse(from,to,bandwidth,num_nodes,num_nodes));
        table_demand = full(sparse(from,to,table,num_nodes,num_nodes));
        
        requests.request_queue(req_id).num_nodes = num_nodes;
        requests.request_queue(req_id).num_links = num_links;
        requests.request_queue(req_id).links_from = from;
        requests.request_queue(req_id).links_to = to;
        requests.request_queue(req_id).cpu_demand = round(rand(num_nodes,1) * (cpu_max - cpu_min) + cpu_min);
        requests.request_queue(req_id).links_bandwidth = bandwidth;
        requests.request_queue(req_id).links_table = table;
        requests.request_queue(req_id).band_demand = band_demand + band_demand';
        requests.request_queue(req_id).table_demand = table_demand + table_demand';
        requests.request_queue(req_id).extra_table_demand = requests.request_queue(req_id).table_demand > 0;
%         requests.request_queue(req_id).cand_sn = cand_sn;
        requests.request_queue(req_id).time_arrival = arrival_times(req_id);
        requests.request_queue(req_id).time_holding = duration_times(req_id);
        requests.request_queue(req_id).time_depart = arrival_times(req_id) + duration_times(req_id);
        
        req_id = req_id + 1;
    end
    
    %     fig = figure(req_set_id);
    
    %     bar(selected_times);
    
    req_id = req_id - 1;
    erlangs = round(mu / lambda);
    requests.arrival_rate = 1 / lambda;
    requests.average_hold_time = mu;
    requests.erlangs = erlangs;
    requests.node_req = [cpu_min cpu_max];
    requests.link_req = [band_min band_max];
%     requests.cand_num = [cand_min cand_max];
    requests.node_req = [2 10];
    fprintf('%d requests were generated, the arrival rate is %.4f, the average holding time is %d.\n',req_id,1/lambda,mu);
    
    savefilename = sprintf('./requests/request-set-%d.mat',round(erlangs));
    fprintf('save requests to file %s\n\n',savefilename);
    save(savefilename,'requests');
    
    
    req_set = req_set + 1;
    
    clear requests
end

