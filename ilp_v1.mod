/* This file shows how to model a virtual network embedding problem in GMPL. */
/* VNE considering flow table resource constraints intermediate nodes */

/* written by Huibai Huang */

# result file
param result_file, symbolic := "result.txt";

set V;
/* set of substrate vertices */

set R;
/*set of k requests*/

set VV{k in R};
/* virtual nodes of k-th request */


param c{u in V};
/* cpu of node u */

param t{u in V};
/* flow table  of node u */

param b{u in V, v in V  };
/* bandwidth of edge (u,v) */

param linkcon1{u in V, v in V};
/* parameter of rho constraint */

param linkcon2{k in R, i in VV[k], j in VV[k]};
/* parameter of rho constraint of k-th request*/

param cd{k in R,i in VV[k]};
/* cpu demand of virtual node VV(i) of k-th request*/

param td{k in R, i in VV[k],j in VV[k]};
/* flow table demand of virtual link VV(i,j) for intermediate nodes of k-th request*/

param tx{k in R,i in VV[k]};
/* table demand of virtual node VV(i) of k-th request */

param bd{k in R, i in VV[k],j in VV[k]};
/* bandwidth demand of virtual link VE(i,j) of k-th request*/

param tB;
/* total bandwidth */
param tC;
/* total CPU*/
param tT;
/* total tables*/

var del{k in R,i in VV[k], u in V}, binary;
/* indicator variable */

var rho{k in R, i in VV[k],j in VV[k], u in V, v in V}, binary;
/* indicator variable */

var omega{k in R, i in VV[k],j in VV[k], u in V}, binary;

minimize cost: (sum{k in R, i in VV[k],j in VV[k], u in V, v in V} rho[k,i,j,u,v]*bd[k,i,j]) / tB + (sum{k in R,i in VV[k], u in V} del[k,i,u] * cd[k, i]) / tC
                    + ( sum{k in R, u in V, i in VV[k],j in VV[k]} omega[k,i,j,u]*td[k,i,j] +  sum{k in R, u in V, i in VV[k]} del[k, i, u] * tx[k, i] ) / tT;


s.t. bwcon1{u in V,v in V}: b[u,v] - sum{k in R, i in VV[k], j in VV[k]} rho[k,i,j,u,v] * bd[k,i,j] >=0;
/* bandwidth constraint*/

s.t. cpucon2{u in V}: c[u] - sum{k in R, i in VV[k]} del[k,i,u] * cd[k,i] >=0;
/* cpu constraint*/

s.t. tablecon3{u in V}: t[u] - (sum{k in R, i in VV[k], j in VV[k]} omega[k,i,j,u]*td[k,i,j] +  sum{k in R, i in VV[k]} del[k,i,u] * tx[k,i] )>=0;
/*flow table constraint*/

s.t. node1{k in R, i in VV[k]}: sum{u in V} del[k,i,u] = 1;
s.t. node2{u in V, k in R}: sum{i in VV[k]} del[k,i,u] <=1;

s.t. flowcon1{u in V, k in R, i in VV[k], j in VV[k]}: sum{v in V} rho[k,i,j,u,v] - sum{v in V} rho[k,i,j,v,u] = del[k,i,u] - del[k,j,u];
/*flow conservation constraints */

s.t. pathcon1 { u in V, v in V, k in R, i in VV[k], j in VV[k]}: rho[k,i,j,u,v] = rho[k,j,i,v,u];

s.t. intercon1{u in V, k in R, i in VV[k], j in VV[k] diff {i}}:sum{v in V diff {u} } rho[k,i,j,u,v] - omega[k,i,j,u] = del[k,i,u];

s.t. rhocon1 { u in V, v in V, k in R, i in VV[k], j in VV[k]}: rho[k,i,j,u,v] <= linkcon1[u,v];
s.t. rhocon2 { u in V, v in V, k in R, i in VV[k], j in VV[k]}: rho[k,i,j,u,v] <= linkcon2[k,i,j];
s.t. omegacon1 { u in V, k in R, i in VV[k], j in VV[k]}: omega[k,i,j,u] <=  linkcon2[k,i,j];

solve;

printf "variable list:\n" > result_file;

printf "V:\n" >> result_file;
printf  {u in V}: "%i ", u >> result_file;
printf "\n" >> result_file;

printf "VV:\n" >> result_file;
printf  {k in R,i in VV[k]}: "%i %i\n",k, i >> result_file;

printf "c:\n" >> result_file;
printf  {u in V}: "%i ", c[u] >> result_file;
printf "\n" >> result_file;

printf "t:\n" >> result_file;
printf  {i in V}: "%i ", t[i] >> result_file;
printf "\n" >> result_file;

printf "b:\n" >> result_file;
printf  {u in V, v in V}: "%i ", b[u,v] >> result_file;
printf "\n" >> result_file;

printf "tT:\n" >> result_file;
printf  "%i ", tB >> result_file;
printf "\n" >> result_file;

printf "del:\n" >> result_file;
printf  {k in R, i in VV[k], u in V: del[k,i,u] > 0}: "%i %i %i %i\n", k,i, u, del[k,i,u] >> result_file;

printf "rho:\n" >> result_file;
printf  {k in R, i in VV[k], j in VV[k], u in V, v in V: rho[k,i,j,u,v] > 0}: "%i %i %i %i %i %i\n", k, i, j, u, v, rho[k,i,j,u,v] >> result_file;

printf "omega:\n" >> result_file;
printf {k in R, i in VV[k], j in VV[k], u in V: omega[k,i,j,u] > 0}: "%i %i %i %i %i\n",k,i ,j, u, omega[k,i,j,u] >> result_file;

printf "usedbandwidth: %f \n", sum{k in R,i in VV[k],j in VV[k], u in V, v in V} rho[k,i,j,u,v]*bd[k,i,j] >> result_file;
printf "usedcpu: %f \n", sum{k in R, i in VV[k], u in V} del[k,i,u] * cd[k,i] >> result_file;
printf "usedflowtable: %f \n",  sum{u in V, k in R, i in VV[k],j in VV[k]} omega[k,i,j,u]*td[k,i,j] + sum{u in V, k in R, i in VV[k]} del[k,i,u] * tx[k,i] >> result_file;

end;
