clear 
currPath = fileparts(mfilename('fullpath'));
requests_folder =[currPath,'\Req-Generator\requests'];
substrate_topo_folder = [currPath,'\Topo-Generator\topo'];
request_file_lists = dir(requests_folder);
request_files = {};
request_set_num  = 0;
for f_id = 1:length(request_file_lists)
     [path_name,file_name,ext] = fileparts(request_file_lists(f_id).name);
     if(strcmp(ext,'.mat'))
           request_set_num = request_set_num + 1;
           request_files = [request_files;request_file_lists(f_id).name];
    end
end
request_file = [requests_folder,'\',request_files{1}];
substrate_file = [substrate_topo_folder,'\topo_sub_test.mat'];
subNet_load = load(substrate_file,'subNet');% load initial substrate network
requests_load = load(request_file,'requests');
subNet = subNet_load.subNet;
requests = requests_load.requests;
request_queue = requests.request_queue;
%[solution,subNet_o] = f_ilp(request,subNet);
%serve the virtual network request
request_total_num = 2;
for req_id = 1 : request_total_num
    request = request_queue(req_id);
    [solution,subNet_o] = f_ilp(request,subNet);
    subNet = subNet_o;
end


