% make substrate topology
clear
curFilename = mfilename('fullpath');
[pathstr, name, ext] = fileparts(curFilename);
sub_txt_folder = [pathstr,'\substrate'];
file_list = dir(sub_txt_folder);


for i = 1:length(file_list)
    filename = file_list(i).name;
	[p_str,name,ext] = fileparts(filename);
	if (strcmp(ext,'.txt') == 0)
	    continue;
	end
	
	full_filename = [sub_txt_folder,'\',filename];
	
	sub_txt_fp = fopen(full_filename,'r');
	
	tline = fgetl(sub_txt_fp);
    tline = fgetl(sub_txt_fp);
	str = deblank(tline);
	
	S = regexp(str, '\s+', 'split');
	
	num_nodes = str2num(S{1});
	num_links = str2num(S{2});
	
	coordinate = zeros(num_nodes,2);
	topology = zeros(num_nodes);
	tline = fgetl(sub_txt_fp);
    tline = fgetl(sub_txt_fp);
	fprintf('\ntopology %d:\n # of nodes: %d, # of links: %d\n\n',i - 2,num_nodes,num_links);
	for j = 1:num_nodes
	   tline = fgetl(sub_txt_fp);
	   str = deblank(tline);
	   S = regexp(str, '\s+', 'split');
	   
	   coordinate(j,1) = str2num(S{1});
	   coordinate(j,2) = str2num(S{2});
       
       fprintf('node %d: (%d,%d)\n',j,coordinate(j,1),coordinate(j,2));
	end
	tline = fgetl(sub_txt_fp);
    
	for j = 1:num_links
	   tline = fgetl(sub_txt_fp);
	   str = deblank(tline);
	   S = regexp(str, '\s+', 'split');
	   node_1 = str2num(S{1}) + 1;
	   node_2 = str2num(S{2}) + 1;
	   
	   topology(node_1,node_2) = 1;
       
       fprintf('link %d: <%d,%d>\n',j,node_1,node_2);
	end
	topology = topology + topology';
	
	subNet.coordinate = coordinate;
	subNet.topology = topology;
	fclose(sub_txt_fp);
    
    degs = sum(topology);%求每个节点的度
    fprintf('\nmin deg: %d\nmean deg: %.2f\nmax deg: %d\n\n',min(degs),mean(degs),max(degs));
    save_filename = [sub_txt_folder,'\..\topo_basic\topo_basic_',name];
    save(save_filename,'subNet');
end