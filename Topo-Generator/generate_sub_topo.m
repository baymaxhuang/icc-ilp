clear
%substrate network parameter
cpu_init_min = 50;% minimum cpu capacity
cpu_init_max = 100;% maximum cpu capacity

band_init_min = 50;% minimum bandwidth capacity
band_init_max = 100;% maximum bandwidth capacity

table_init_min = 50;% minimum flow table capacity
table_init_max = 100;% maximum flow table capacity

% get the full path of the current m-file
curFilename = mfilename('fullpath');
[pathstr, name, ext] = fileparts(curFilename);
topo_basic_folder = [pathstr,'\topo_basic'];
file_list = dir(topo_basic_folder);
for f_id = 1:length(file_list)
    filename = file_list(f_id).name;
	[p_str,name,ext] = fileparts(filename);
	if (strcmp(ext,'.mat') == 0)
	    continue;
	end
    %load a substrate topology randomly
    sub_Load = load([topo_basic_folder,'\',filename]);
    subNet = sub_Load.subNet;

    %generate the substrate network
    subNet.num_nodes = size(subNet.topology,1);
    subNet.num_links = length(find(subNet.topology(:) ~= 0)) / 2;
    sub_num_nodes = subNet.num_nodes;
    sub_num_links = subNet.num_links;
    subNet.cpu_capacity = round(rand(sub_num_nodes,1) * (cpu_init_max - cpu_init_min) + cpu_init_min);%在允许范围内分配节点资源
    subNet.table_capacity = round(rand(sub_num_nodes,1) * (table_init_max - table_init_min) + table_init_min);
    subNet.band_matrix = zeros(sub_num_nodes);
    subNet.node_capacity = [cpu_init_min cpu_init_max];
    %
    subNet.band_capacity = [band_init_min band_init_max];
    degree_set = sum(subNet.topology);
    subNet.degree = [min(degree_set) mean(degree_set) max(degree_set)];
    fprintf('min degree:%d \nmean degree:%.4f\nmax degree:%d\n\n',min(degree_set),mean(degree_set), max(degree_set));
    %分配链路资源
    for i = 1:(sub_num_nodes - 1)
        for j = (i + 1):sub_num_nodes
            if subNet.topology(i,j) == 1
                subNet.band_matrix(i,j) = round(rand()*(band_init_max - band_init_min) + band_init_min);
                subNet.band_matrix(j,i) = subNet.band_matrix(i,j);
            end
        end
    end
    S = regexp(name, 'basic_', 'split');
    substr = S{2};
    save_filename = [pathstr,'\topo\topo_',substr];
    save(save_filename,'subNet');
end