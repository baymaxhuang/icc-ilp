clear all;
close all;
%%substrate node
n = 6;%number of substrate network nodes
V = 1:n;
c=[ 6 8 5 5 10 6];%cpu 
% c = [100 100 100 100 100 100];
%flow table
t=[ 6 8 5 5 9 6];
% t=[ 100 100 100 100 100 100];
%bandwidth
b=[0 4 4 0 0 0;
     4 0 3 3 4 0;
     4 3 0 0 5 0;
     0 3 0 0 4 5;
     0 4 5 4 0 6;
     0 0 0 5 6 0];
% b=[0 100 100 0 0 0;
%      100 0 100 100 100 0;
%      100 100 0 0 100 0;
%      0 100 0 0 100 100;
%      0 100 100 100 0 100;
%      0 0 0 100 100 0];
 tB = sum(sum(b));
 tC = sum(c);
 tT = sum(t);
 
 %%virtual network 
 req_num = 2;
 VV = cell(1,req_num);
 CD = cell(1,req_num);
 BD = cell(1,req_num);
 TX = cell(1,req_num);
 TD = cell(1,req_num);
 LC2 = cell(1,req_num);
 R = 1:req_num;%request number set
 V_N = [3 3]; %node number set of virtual network
 cd = [5 5 5];
 bd =  [0 4 4;
           4 0 4;
           4 4 0];
 tx =  [5 5 5];
 td =  [0 2 2;
          2 0 2;
          2 2 0];
 linkcon1 = b > 0;
 linkcon2 = bd > 0;
 for k = 1:req_num
      VV{k} = 1:V_N(k); %node set of k-th request
      CD{k} = cd;
      BD{k} = bd;
      TX{k} = tx;
      TD{k} = td;
      LC2{k} = linkcon2;
 end

%%%%%% create a data file %%%%%%%%%%%%
date_filename = 'ilp_vne_v1.dat';
fp = fopen(date_filename, 'w');
fprintf(fp, 'data;\n');

%%substrate node
fprintf(fp,'set V := \n');
fprintf(fp,'%d\n',1:(n-1));
fprintf(fp,'%d;\n',n);

%%cpu of substrate node 
fprintf(fp,'param c := \n');
for i = 1:(n-1)
    fprintf(fp,'%d  %d\n', i, c(i));
end
fprintf(fp,'%d  %d;\n', n, c(n));

%%flow table of substrate node
fprintf(fp,'param t := \n');
for i = 1:(n-1)
    fprintf(fp,'%d  %d\n', i, t(i));
end
fprintf(fp,'%d  %d;\n', n, t(n));

%%bandwidth of substrate network
fprintf(fp,'param b :');
fprintf(fp,' %d ',1:n);
fprintf(fp,':=\n');
for i = 1:(n-1)
    fprintf(fp,'%d  %d  ', i, b(i, 1:(n-1)));
    fprintf(fp,'%d\n',b(i,n));
end
fprintf(fp,'%d  %d  ', n, b(n, 1:(n-1)));
fprintf(fp,'%d;\n',b(n,n));

%%request set
fprintf(fp,'set R := \n');
fprintf(fp,'%d\n',1:(req_num-1));
fprintf(fp,'%d;\n',req_num);

%virtual network node set
for k = 1:req_num
    v_n = V_N(k);
    fprintf(fp,'set VV[%d] := \n',k);
    fprintf(fp,'%d\n',1:(v_n-1));
    fprintf(fp,'%d;\n',v_n);
end

%%cpu demand of virtual network
fprintf(fp,'param cd := \n');
for k = 1:req_num
    v_n = V_N(k);
    cd = CD{k};
    for i = 1:v_n
        if k ==req_num && i == v_n
            fprintf(fp,'%d %d  %d;\n', k, i, cd(i));
        else
            fprintf(fp,'%d %d  %d\n', k, i, cd(v_n));
        end
    end
end

%%bandwidth demand of virtual network
fprintf(fp,'param bd :=\n');
for k = 1: req_num
    v_n = V_N(k);
    bd = BD{k};
    for i = 1: v_n
        for j = 1:v_n
            if k == req_num && i == v_n && j == v_n
                 fprintf(fp,'%d %d  %d %d;\n',k, i, j, bd(i, j));
            else
                fprintf(fp,'%d %d  %d %d\n',k, i, j, bd(i, j));
            end
        end
    end
end

%%flowtable demand of virtual network for intermediate node
fprintf(fp,'param td :=\n');
for k = 1: req_num
    v_n = V_N(k);
    td = TD{k};
    for i = 1: v_n
        for j = 1:v_n
            if k == req_num && i == v_n && j == v_n
                 fprintf(fp,'%d %d  %d %d;\n',k, i, j, td(i, j));
            else
                fprintf(fp,'%d %d  %d %d\n',k, i, j, td(i, j));
            end
        end
    end
end

%%flow table  demand of virtual network for edge node
fprintf(fp,'param tx := \n');
for k = 1:req_num
    v_n = V_N(k);
    tx = TX{k};
    for i = 1:v_n
        if k ==req_num && i == v_n
            fprintf(fp,'%d %d  %d;\n', k, i, tx(i));
        else
            fprintf(fp,'%d %d  %d\n', k, i, tx(v_n));
        end
    end
end


%%linkcon1 
fprintf(fp,'param linkcon1 :=\n');
for i = 1:n
    for j = 1:n
        if i ==n && j == n
            fprintf(fp,'%d  %d %d;\n', i, j, linkcon1(i, j));
        else
            fprintf(fp,'%d  %d %d\n', i, j, linkcon1(i, j));
        end
    end
end

%%linkcon2
fprintf(fp,'param linkcon2 :=\n');
for k = 1: req_num
    v_n = V_N(k);
     linkcon2 = LC2{k};
    for i = 1: v_n
        for j = 1:v_n
            if k == req_num && i == v_n && j == v_n
                 fprintf(fp,'%d %d  %d %d;\n',k, i, j, linkcon2(i, j));
            else
                fprintf(fp,'%d %d  %d %d\n',k, i, j, linkcon2(i, j));
            end
        end
    end
end

fprintf(fp,'param tB := \n');
fprintf(fp,'%d;\n',tB);

fprintf(fp,'param tC := \n');
fprintf(fp,'%d;\n',tC);

fprintf(fp,'param tT := \n');
fprintf(fp,'%d;\n',tT);

%%%%%%solve ilp%%%%%%%%%%%%
system('glpsol --model ilp_v1.mod --data ilp_vne_v1.dat --output ilp_for_VNE_v1.sol');

fclose(fp);
