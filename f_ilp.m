function  [solution,subNet_o] = f_ilp(request,subNet)
%map the virtual network request to the substrate network and update the substrate network information
subNet_o = subNet; % save the original state of the substrate
%virtual network requirement
v_node_num = request.num_nodes;% get the number of virtual nodes
v_link_num = request.num_links;% get the number of virtual links
v_band_demand = request.band_demand;%
v_table_demand = request.table_demand;
v_extra_table_demand = request.extra_table_demand;
v_cpu_demand= request.cpu_demand;% get the computing resource that needed by each virtual node

%cand_sns = request.cand_sn;% get the sets of candidate substrate nodes
links_from = request.links_from;% starting node of each virtual link
links_to = request.links_to;% ending node of each virtual link
%substrate network
s_node_num = subNet.num_nodes;% get the node number of the substrate
band_matrix = subNet.band_matrix;% get the bandwidth matrix
cpu_capacity = subNet.cpu_capacity;% get the computing resource of each substrate node
table_capacity = subNet.table_capacity;

%files for GLPK
mod_filename = 'ilp_v2.mod';
dat_filename = 'ilp_vne_v2.dat';
sol_filename = 'ilp_for_VNE_v2.sol';
lim_running_time = 3600;% maximum execution time

 linkcon1 = band_matrix > 0;
 linkcon2 = v_band_demand > 0;
 tB = sum(sum(band_matrix));
 tC = sum(cpu_capacity);
 tT = sum(table_capacity);
%%%%%% create a data file %%%%%%%%%%%%
date_filename = 'ilp_vne_v2.dat';
fp = fopen(date_filename, 'w');
fprintf(fp, 'data;\n');

%substrate node
fprintf(fp,'set V := \n');
fprintf(fp,'%d\n',1:(s_node_num-1));
fprintf(fp,'%d;\n',s_node_num);

%%virtual network node
fprintf(fp,'set VV := \n');
fprintf(fp,'%d\n',1:(v_node_num-1));
fprintf(fp,'%d;\n',v_node_num);

%cpu of substrate node 
fprintf(fp,'param c := \n');
for i = 1:(s_node_num-1)
    fprintf(fp,'%d  %d\n', i, cpu_capacity(i));
end
fprintf(fp,'%d  %d;\n', s_node_num, cpu_capacity(s_node_num));

%flow table of substrate node
fprintf(fp,'param t := \n');
for i = 1:(s_node_num -1)
    fprintf(fp,'%d  %d\n', i, table_capacity(i));
end
fprintf(fp,'%d  %d;\n', s_node_num, table_capacity(s_node_num));

%bandwidth of substrate network
fprintf(fp,'param b :');
fprintf(fp,' %d ',1:s_node_num);
fprintf(fp,':=\n');
for i = 1:(s_node_num-1)
    fprintf(fp,'%d  %d  ', i, band_matrix(i, 1:(s_node_num-1)));
    fprintf(fp,'%d\n', band_matrix(i,s_node_num));
end
fprintf(fp,'%d  %d  ', s_node_num, band_matrix(s_node_num, 1:(s_node_num-1)));
fprintf(fp,'%d;\n',band_matrix(s_node_num,s_node_num));

%cpu demand of virtual network
fprintf(fp,'param cd := \n');
for i = 1:(v_node_num-1)
    fprintf(fp,'%d  %d\n', i, v_cpu_demand(i));
end
fprintf(fp,'%d  %d;\n', v_node_num, v_cpu_demand(v_node_num));

%%bandwidth demand of virtual network
fprintf(fp,'param bd :');
fprintf(fp,' %d ',1:v_node_num);
fprintf(fp,':=\n');
for i = 1:(v_node_num-1)
    fprintf(fp,'%d  %d  ', i, v_band_demand(i, 1:(v_node_num-1)));
    fprintf(fp,'%d\n',v_band_demand(i,v_node_num));
end
fprintf(fp,'%d  %d  ', v_node_num, v_band_demand(v_node_num, 1:(v_node_num-1)));
fprintf(fp,'%d;\n',v_band_demand(v_node_num,v_node_num));

%flowtable demand of virtual network
fprintf(fp,'param td :');
fprintf(fp,' %d ',1:v_node_num);
fprintf(fp,':=\n');
for i = 1:(v_node_num-1)
    fprintf(fp,'%d  %d  ', i, v_table_demand(i, 1:(v_node_num-1)));
    fprintf(fp,'%d\n',v_table_demand(i,v_node_num));
end
fprintf(fp,'%d  %d  ', v_node_num, v_table_demand(v_node_num, 1:(v_node_num-1)));
fprintf(fp,'%d;\n',v_table_demand(v_node_num,v_node_num));

%extra flowtable demand of ingress node of substrate path representing the
%virtual link
fprintf(fp,'param tx :');
fprintf(fp,' %d ',1:v_node_num);
fprintf(fp,':=\n');
for i = 1:(v_node_num-1)
    fprintf(fp,'%d  %d  ', i, v_extra_table_demand(i, 1:(v_node_num-1)));
    fprintf(fp,'%d\n',v_extra_table_demand(i,v_node_num));
end
fprintf(fp,'%d  %d  ', v_node_num, v_extra_table_demand(v_node_num, 1:(v_node_num-1)));
fprintf(fp,'%d;\n',v_extra_table_demand(v_node_num,v_node_num));

%linkcon1 
fprintf(fp,'param linkcon1 :');
fprintf(fp,' %d ',1:s_node_num);
fprintf(fp,':=\n');
for i = 1:(s_node_num-1)
    fprintf(fp,'%d  %d  ', i, linkcon1(i, 1:(s_node_num-1)));
    fprintf(fp,'%d\n',linkcon1(i,s_node_num));
end
fprintf(fp,'%d  %d  ', s_node_num, linkcon1(s_node_num, 1:(s_node_num-1)));
fprintf(fp,'%d;\n',linkcon1(s_node_num,s_node_num));

%linkcon2
fprintf(fp,'param linkcon2 :');
fprintf(fp,' %d ',1:v_node_num);
fprintf(fp,':=\n');
for i = 1:(v_node_num-1)
    fprintf(fp,'%d  %d  ', i, linkcon2(i, 1:(v_node_num-1)));
    fprintf(fp,'%d\n',linkcon2(i,v_node_num));
end
fprintf(fp,'%d  %d  ', v_node_num, linkcon2(v_node_num, 1:(v_node_num-1)));
fprintf(fp,'%d;\n',linkcon2(v_node_num,v_node_num));


fprintf(fp,'param tB := \n');
fprintf(fp,'%d;\n',tB);

fprintf(fp,'param tC := \n');
fprintf(fp,'%d;\n',tC);

fprintf(fp,'param tT := \n');
fprintf(fp,'%d;\n',tT);
% end the data file
fprintf(fp,'end;\n\n');
fclose(fp);

%%%%%%solve ilp%%%%%%%%%%%%
%system('glpsol --model ilp_v2.mod --data ilp_vne_v2.dat --output ilp_for_VNE_v2.sol');
% call GLPK to solve the LP
command_string = sprintf('glpsol --model %s --data %s --output %s --tmlim %d',mod_filename,dat_filename,sol_filename,lim_running_time);
[status, result] = system(command_string,'-echo');

% check whether the LP has been solved successfully
fp_check = fopen(sol_filename,'r');
if fp_check == -1
    fprintf('Error Occurs in Your LP\n');
    solution.state = -1;
    return;
end
% check whether the solution is optimal
check_str = fgetl(fp_check);
while ~feof(fp_check) && strncmp(check_str,'Status',6) == 0
    check_str = fgetl(fp_check);
end
if strcmp(check_str,'Status:     INTEGER OPTIMAL') == 0
    solution.state = 0;
    fclose(fp_check);
    return;
end
fclose(fp_check);
solution.state = 1;
% load the solution
delta = load('solution\delta.txt');
omega = load('solution\omega.txt');
rho = load('solution\rho.txt');
flowtable_use = load('solution\flowtable.txt');
%update substrate network information
%update node cpu capacity
cpu_capacity(delta(:,2)) = cpu_capacity(delta(:,2)) - v_cpu_demand(delta(:,1));
subNet_o.cpu_capacity = cpu_capacity;
%update bandwidth information
for i = 1 : size(rho,1)
    band_matrix(rho(i,3),rho(i,4)) = band_matrix(rho(i,3),rho(i,4)) - v_table_demand(rho(i,1),rho(i,2));
end
subNet_o.band_matrix = band_matrix;
%update table information
for i = 1 : size(flowtable_use,1)
    table_capacity(flowtable_use(i,1)) = table_capacity(flowtable_use(i,1)) - flowtable_use(i,2);
    if table_capacity(flowtable_use(i,1)) < 0
        fprintf('ipl flow table solution error');
    end
end
subNet_o.table_capacity = table_capacity;
end

