clear all;
close all;
mod_filename = 'ilp_v2.mod';
dat_filename = 'ilp_vne_v2.dat';
sol_filename = 'ilp_for_VNE_v2.sol';
lim_running_time = 3600;% maximum execution time
%substrate node
%number of substrate network nodes
n1 = 6;
V = 1:n1;

%cpu 
c=[ 6 8 4 5 7 6];
% c = [100 100 100 100 100 100];
%flow table
t=[ 6 12 4 7 10 10];
% t=[ 100 100 100 100 100 100];
%bandwidth
b=[0 4 4 0 0 0;
     4 0 6 3 4 0;
     4 6 0 0 5 0;
     0 3 0 0 4 5;
     0 4 5 4 0 6;
     0 0 0 5 6 0];
% b=[0 100 100 0 0 0;
%      100 0 100 100 100 0;
%      100 100 0 0 100 0;
%      0 100 0 0 100 100;
%      0 100 100 100 0 100;
%      0 0 0 100 100 0];
 tB = sum(sum(b));
 tC = sum(c);
 tT = sum(t);
 
 %virtual node
 v_n1 = 4;
 VV = 1:v_n1;
 cd = [5 5 5 5];
%  bd =  [0 4 4;
%            4 0 4;
%            4 4 0];
%  td =  [0 1 1;
%           1 0 1;
%           1 1 0];
%   tx =  [0 1 1;
%            1 0 1;
%            1 1 0];
%  bd =  [0 4 0;
%            4 0 4;
%            0 4 0];
%  td =  [0 1 0;
%           1 0 1;
%           0 1 0];
%   tx =  [0 1 0;
%            1 0 1;
%            0 1 0];
 bd =  [0 4 4 4;
           4 0 4 0;
           4 4 0 0;
           4 0 0 0];
 td =  [0 1 1 1;
          1 0 1 0;
          1 1 0 0
          1 0 0 0];
  tx =  [0 1 1 1;
           1 0 1 0;
           1 1 0 0
           1 0 0 0];
 linkcon1 = b > 0;
 linkcon2 = bd > 0;
%%%%%% create a data file %%%%%%%%%%%%
date_filename = 'ilp_vne_v2.dat';
fp = fopen(date_filename, 'w');
fprintf(fp, 'data;\n');

%substrate node
fprintf(fp,'set V := \n');
fprintf(fp,'%d\n',1:(n1-1));
fprintf(fp,'%d;\n',n1);

%%virtual network node
fprintf(fp,'set VV := \n');
fprintf(fp,'%d\n',1:(v_n1-1));
fprintf(fp,'%d;\n',v_n1);

%cpu of substrate node 
fprintf(fp,'param c := \n');
for i = 1:(n1-1)
    fprintf(fp,'%d  %d\n', i, c(i));
end
fprintf(fp,'%d  %d;\n', n1, c(n1));

%flow table of substrate node
fprintf(fp,'param t := \n');
for i = 1:(n1-1)
    fprintf(fp,'%d  %d\n', i, t(i));
end
fprintf(fp,'%d  %d;\n', n1, t(n1));

%bandwidth of substrate network
fprintf(fp,'param b :');
fprintf(fp,' %d ',1:n1);
fprintf(fp,':=\n');
for i = 1:(n1-1)
    fprintf(fp,'%d  %d  ', i, b(i, 1:(n1-1)));
    fprintf(fp,'%d\n',b(i,n1));
end
fprintf(fp,'%d  %d  ', n1, b(n1, 1:(n1-1)));
fprintf(fp,'%d;\n',b(n1,n1));

%cpu demand of virtual network
fprintf(fp,'param cd := \n');
for i = 1:(v_n1-1)
    fprintf(fp,'%d  %d\n', i, cd(i));
end
fprintf(fp,'%d  %d;\n', v_n1, cd(v_n1));

%%bandwidth demand of virtual network
fprintf(fp,'param bd :');
fprintf(fp,' %d ',1:v_n1);
fprintf(fp,':=\n');
for i = 1:(v_n1-1)
    fprintf(fp,'%d  %d  ', i, bd(i, 1:(v_n1-1)));
    fprintf(fp,'%d\n',bd(i,v_n1));
end
fprintf(fp,'%d  %d  ', v_n1, bd(v_n1, 1:(v_n1-1)));
fprintf(fp,'%d;\n',bd(v_n1,v_n1));

%flowtable demand of virtual network
fprintf(fp,'param td :');
fprintf(fp,' %d ',1:v_n1);
fprintf(fp,':=\n');
for i = 1:(v_n1-1)
    fprintf(fp,'%d  %d  ', i, td(i, 1:(v_n1-1)));
    fprintf(fp,'%d\n',td(i,v_n1));
end
fprintf(fp,'%d  %d  ', v_n1, td(v_n1, 1:(v_n1-1)));
fprintf(fp,'%d;\n',td(v_n1,v_n1));

%extra flowtable demand of ingress node of substrate path representing the
%virtual link
fprintf(fp,'param tx :');
fprintf(fp,' %d ',1:v_n1);
fprintf(fp,':=\n');
for i = 1:(v_n1-1)
    fprintf(fp,'%d  %d  ', i, tx(i, 1:(v_n1-1)));
    fprintf(fp,'%d\n',tx(i,v_n1));
end
fprintf(fp,'%d  %d  ', v_n1, tx(v_n1, 1:(v_n1-1)));
fprintf(fp,'%d;\n',tx(v_n1,v_n1));

%linkcon1 
fprintf(fp,'param linkcon1 :');
fprintf(fp,' %d ',1:n1);
fprintf(fp,':=\n');
for i = 1:(n1-1)
    fprintf(fp,'%d  %d  ', i, linkcon1(i, 1:(n1-1)));
    fprintf(fp,'%d\n',linkcon1(i,n1));
end
fprintf(fp,'%d  %d  ', n1, linkcon1(n1, 1:(n1-1)));
fprintf(fp,'%d;\n',linkcon1(n1,n1));

%linkcon2
fprintf(fp,'param linkcon2 :');
fprintf(fp,' %d ',1:v_n1);
fprintf(fp,':=\n');
for i = 1:(v_n1-1)
    fprintf(fp,'%d  %d  ', i, linkcon2(i, 1:(v_n1-1)));
    fprintf(fp,'%d\n',linkcon2(i,v_n1));
end
fprintf(fp,'%d  %d  ', v_n1, linkcon2(v_n1, 1:(v_n1-1)));
fprintf(fp,'%d;\n',linkcon2(v_n1,v_n1));


fprintf(fp,'param tB := \n');
fprintf(fp,'%d;\n',tB);

fprintf(fp,'param tC := \n');
fprintf(fp,'%d;\n',tC);

fprintf(fp,'param tT := \n');
fprintf(fp,'%d;\n',tT);
% end the data file
fprintf(fp,'end;\n\n');
fclose(fp);

%%%%%%solve ilp%%%%%%%%%%%%
%system('glpsol --model ilp_v2.mod --data ilp_vne_v2.dat --output ilp_for_VNE_v2.sol');
% call GLPK to solve the LP
command_string = sprintf('glpsol --model %s --data %s --output %s --tmlim %d',mod_filename,dat_filename,sol_filename,lim_running_time);
[status, result] = system(command_string,'-echo');

% check whether the LP has been solved successfully
fp_check = fopen(sol_filename,'r');
if fp_check == -1
    fprintf('Error Occurs in Your LP\n');
    return;
end
% check whether the solution is optimal
check_str = fgetl(fp_check);
while ~feof(fp_check) && strncmp(check_str,'Status',6) == 0
    check_str = fgetl(fp_check);
end
if strcmp(check_str,'Status:     INTEGER OPTIMAL') == 0
    %solution.state = 0;
    fclose(fp_check);
    return;
end
fclose(fp_check);

